package com.demo.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
public class Book {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "You must provide books's name")
	private String name;

	@ManyToOne
	@JoinColumn(name = "AUTHOR_ID")
	@JsonInclude(value = Include.NON_NULL)
	private Author author;

	@NotEmpty(message = "You must provide books's pages")
	private int pages;

	@NotEmpty(message = "You must provide books's language")
	private Language language;

	@NotEmpty(message = "You must provide books's publication date")
	@JsonFormat(pattern = "MM/dd/yyyy")
	private Date publicationDate;

	public Book() {
	}

	/**
	 * @param name
	 * @param pages
	 * @param language
	 * @param publicationDate
	 */
	public Book(String name, int pages, Language language, Date publicationDate) {
		this.name = name;
		this.pages = pages;
		this.language = language;
		this.publicationDate = publicationDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public Date getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(Date publicationDate) {
		this.publicationDate = publicationDate;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Book [id=" + id + ", name=" + name + ", author=" + author + ", pages=" + pages + ", language="
				+ language + ", publicationDate=" + publicationDate + "]";
	}

}
