package com.demo.domain;

public enum Language {
	
	ENGLISH("english"),
	PORTUGUESE("portuguese"),
	FRENCH("french"),
	GERMAN("german");
	
	private String language;

	private Language(String language) {
		this.language = language;
	}

	public String getLanguage() {
		return language;
	}
	
	
}
